package org.apache.shardingsphere.elasticjob.lite.api.bootstrap.jobtset;

import org.apache.shardingsphere.elasticjob.api.JobConfiguration;
import org.apache.shardingsphere.elasticjob.lite.api.bootstrap.impl.ScheduleJobBootstrap;
import org.apache.shardingsphere.elasticjob.reg.base.CoordinatorRegistryCenter;
import org.apache.shardingsphere.elasticjob.reg.zookeeper.ZookeeperConfiguration;
import org.apache.shardingsphere.elasticjob.reg.zookeeper.ZookeeperRegistryCenter;

/**
 * @author GuangChao.Zhang
 * @version 1.0
 * @date 2023/6/2 14:17
 */
public class MainJava {
    /**
     * @author GuangChao.Zhang
     */

        //mvn clean install -DskipTests -Drat.skip=true -Dmaven.test.skip=true -X -Dmaven.javadoc.skip=true
        public static void main(String[] args) {
            new ScheduleJobBootstrap(coordinatorRegistryCenter(), new TestJob(), createJobConfiguration()).schedule();
        }

        private static CoordinatorRegistryCenter coordinatorRegistryCenter() {
            ZookeeperConfiguration zc = new ZookeeperConfiguration("127.0.0.1:2181", "elastic-job-dev");
            zc.setConnectionTimeoutMilliseconds(40000);
            zc.setMaxRetries(5);
            CoordinatorRegistryCenter registryCenter = new ZookeeperRegistryCenter(zc);
            registryCenter.init();
            return registryCenter;
        }

        private static JobConfiguration createJobConfiguration() {
            String jobs = "0=zgc,1=gqz";
            return JobConfiguration.newBuilder("TestJob", 2)
                    .cron("0/5 * * * * ?")
                    .shardingItemParameters(jobs)
//                    .jobListenerTypes("testJobListener")
                    .overwrite(true)
                    .failover(true)
                    .build();
        }
}
