/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.shardingsphere.elasticjob.infra.handler.sharding.impl;

import com.google.common.collect.Lists;
import org.apache.shardingsphere.elasticjob.infra.handler.sharding.JobInstance;
import org.apache.shardingsphere.elasticjob.infra.handler.sharding.JobShardingStrategy;

import java.util.*;

/**
 * 自定义策略
 */
public final class CustomJobShardingStrategy implements JobShardingStrategy {
    @Override
    public Map<JobInstance, List<Integer>> sharding(List<JobInstance> jobInstances, String jobName, int shardingTotalCount) {
        Map<JobInstance, List<Integer>> result = new LinkedHashMap<>( 1);
        // 判断实例不为空
        if (jobInstances.isEmpty()) {
            return Collections.emptyMap();
        }
        // 先将作业分片加入容器
        ArrayList<Integer> customJobShardingList = Lists.newArrayList();
        for(int i=0;i<shardingTotalCount;i++){
            customJobShardingList.add(i);
        }
        // 将所有分片都放在第一个实例中，
        result.put(jobInstances.get(0),customJobShardingList);

        return result;
    }

    @Override
    public String getType() {
        return "Custom";
    }
}
