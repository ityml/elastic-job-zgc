package org.apache.shardingsphere.elasticjob.infra.listener;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author GuangChao.Zhang
 * @version 1.0
 * @date 2023/6/5 15:43
 */
public class TestJobListener implements ElasticJobListener {

    private static final SimpleDateFormat formatter =new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    // 无参构造器
    public TestJobListener(){}
    @Override
    public void beforeJobExecuted(ShardingContexts shardingContexts) {
        System.out.println(formatter.format(new Date()) + " : 准备数据备份环境。");
    }

    @Override
    public void afterJobExecuted(ShardingContexts shardingContexts) {
        System.out.println(formatter.format(new Date()) + " : 清理数据备份环境。");
    }

    @Override
    public String getType() {
        return "testJobListener";
    }
}
